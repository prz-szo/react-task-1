export const API_URL_PREFIX = 'http://localhost:3003';
export const LOGO_URL = `${API_URL_PREFIX}/photos/1`;
export const AUTHORS_URL = `${API_URL_PREFIX}/authors`;
export const POSTS_URL_PREFIX = `${API_URL_PREFIX}/posts`;
export const LOGIN_URL = `${API_URL_PREFIX}/auth/login`;
export const USER_INFO_URL = `${API_URL_PREFIX}/auth/me`;
