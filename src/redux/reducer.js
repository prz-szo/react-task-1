import { combineReducers } from 'redux';

import postReducer from './modules/post/reducer';
import authorsReducer from './modules/authors/reducer';
import loginReducer from './modules/login/reducer';
import formReducer from './modules/form/reducer';


const rootReducer = combineReducers({
    post: postReducer,
    authors: authorsReducer,
    login: loginReducer,
    form: formReducer
});

export default rootReducer;
