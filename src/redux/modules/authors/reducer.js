import {RECEIVE_AUTHOR_DATA, RECEIVE_AUTHORS} from "../../actions";


const initialState = {
    activeUser: {},
    users: []
};

const authorsReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case RECEIVE_AUTHORS:
            return { ...state, users: action.users };

        case RECEIVE_AUTHOR_DATA:
            return { ...state, activeUser: action.activeUser };

        default:
            return state;
    }
};

export default authorsReducer;
