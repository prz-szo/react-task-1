import { AUTHORS_URL, USER_INFO_URL } from '../../../config/consts';
import { RECEIVE_AUTHOR_DATA, RECEIVE_AUTHORS } from '../../actions';
import { fetchJson } from '../../../utils/fetchJson';

export const fetchAuthors = () => {
    const request = {
        method: 'GET',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json; charset=utf-8',
            Authorization: window.localStorage.getItem('userToken')
        }
    };

    return dispatch => {
        fetchJson(AUTHORS_URL, request, (users) => {
            dispatch(receiveAuthors(users))
        })
    };
};

export const fetchAuthorData = () => {
    const request = {
        method: 'GET',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json; charset=utf-8',
            Authorization: window.localStorage.getItem('userToken')
        }
    };

    return dispatch => {
        fetchJson(USER_INFO_URL, request, (userInfo) => {
            dispatch(receiveAuthorData(userInfo));
        })
    };
};

export const receiveAuthorData = activeUser => ({
    type: RECEIVE_AUTHOR_DATA,
    activeUser
});

export const receiveAuthors = users => ({
    type: RECEIVE_AUTHORS,
    users
});
