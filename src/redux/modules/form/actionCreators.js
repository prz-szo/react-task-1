import { VALIDATION, VALIDATION_TYPES } from '../../actions';

export const clearErrors = () => {
    return {
        type: VALIDATION.CLEAR_VALIDATION_ERRORS
    }
};

export const validationPass = () => {
    return clearErrors();
};

export const validationFail = (errors) => {
    return {
        type: VALIDATION.FAIL,
        errors
    }
};

export const validate = (fieldsToValidate) => {
    const errors = {};

    fieldsToValidate.map(field => errors[field.fieldId] = []);

    let error = {};
    fieldsToValidate.map(field => {
        switch (field.type) {
            case VALIDATION_TYPES.INPUT:
                error = validateTextInput(field);
                break;
            case VALIDATION_TYPES.RADIOGROUP:
                error = validateRadioGroup(field);
                break;
            default:
        }

        return (error) ? errors[error.fieldId].push(error.errorMessage) : null;
    });

    return dispatch => {
        if (!checkIfErrors(errors)) {
            dispatch(validationFail(errors));
            return false;
        }

        dispatch(validationPass());
        return true;
    }
};

export const checkIfErrors = (formErrors) => (
    Object.values(formErrors).map(errorsForField =>
        errorsForField.every(value => !value)
    ).every(value => value)
);

export const validateTextInput = (field) => {
    if (!field.value) {
        return {
            fieldId: field.fieldId,
            errorMessage: 'This field cannot be empty'
        };
    }
};

export const validateRadioGroup = (field) => {
    if (!field.value) {
        return {
            fieldId: field.fieldId,
            errorMessage: 'You must select any item'
        };
    }
};
