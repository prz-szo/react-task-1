import { VALIDATION } from '../../actions';


const initialState = {
    errors: {}
};

const formReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case VALIDATION.FAIL:
            return { ...state, errors: action.errors };

        case VALIDATION.CLEAR_VALIDATION_ERRORS:
            return { ...state, errors: {} };

        default:
            return state;
    }
};

export default formReducer;
