import { LOGIN_ACTIONS } from '../../actions';
import { LOGIN_URL } from "../../../config/consts";
import { fetchJson } from "../../../utils/fetchJson";
import { prepareRequest } from "../../../utils/prepareRequest";


export const logIn = (token) => {
    window.localStorage.setItem('userToken', token);
    return {
        type: LOGIN_ACTIONS.LOG_IN,
        token
    }
};

export const logOut = () => {
    window.localStorage.removeItem('userToken');
    return {
        type: LOGIN_ACTIONS.LOG_OUT
    }
};

export const checkCredentials = (credentials, onSuccess) => {
    const request = {
        ...prepareRequest(credentials),
        method: 'POST'
    };

    return dispatch =>
        fetchJson(`${LOGIN_URL}`, request,
            (response) => {
                if(typeof response === 'object') {
                    dispatch(logIn(response.token));
                    onSuccess();
                } else {
                    dispatch(checkCredentialsFail([response]));
                }
            }
        ).catch(error => dispatch(checkCredentialsFail([error.message])));
};

export const checkCredentialsFail = (errors) => {
    return {
        type: LOGIN_ACTIONS.CHECK_CREDENTIALS_FAIL,
        errors
    }
};
