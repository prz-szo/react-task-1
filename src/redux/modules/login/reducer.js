import { LOGIN_ACTIONS } from '../../actions';


const initialState = {
    errors: []
};

const loginReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case LOGIN_ACTIONS.CHECK_CREDENTIALS_FAIL:
            return { ...state, errors: action.errors };

        case LOGIN_ACTIONS.CHECK_CREDENTIALS:
        case LOGIN_ACTIONS.LOG_OUT:
        case LOGIN_ACTIONS.LOG_IN:
        default:
            return state;
    }
};

export default loginReducer;
