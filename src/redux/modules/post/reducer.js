import { POST_ACTIONS } from '../../actions';


const initialState = {
    filterText: '',
    activePost: null,
    posts: []
};

const postReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case POST_ACTIONS.ADD:
            return { ...state, posts: [...state.posts, action.post] };

        case POST_ACTIONS.SET_ACTIVE:
            return {
                ...state,
                activePost: action.postId
            };

        case POST_ACTIONS.UPDATE:
            return { ...state,
                posts: state.posts.map((post) =>
                    post.id !== action.post.id ? post : action.post
                ),
                activePost: null
            };

        case POST_ACTIONS.DELETE:
            return {
                ...state,
                posts: state.posts.filter((post) =>
                    post.id !== action.postId
                )
            };

        case POST_ACTIONS.CHANGE_FILTERING_TEXT:
            return { ...state, filterText: action.filterText };

        case POST_ACTIONS.RECEIVE:
            return { ...state, posts: action.posts };

        default:
            return state;
    }
};

export default postReducer;
