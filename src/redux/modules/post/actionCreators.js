import { POST_ACTIONS } from '../../actions';
import { fetchJson } from '../../../utils/fetchJson';
import { prepareRequest } from '../../../utils/prepareRequest';

import { POSTS_URL_PREFIX } from '../../../config/consts';


export const createPost = (postData, callback) => {
    delete postData.id;
    const request = { ...prepareRequest(postData), method: 'POST'};
    const POST_URL = `${POSTS_URL_PREFIX}`;

    return dispatch =>
        fetchJson(POST_URL, request,
            (response) => {
                postData = { ...postData, id: response.id };
                dispatch(addPost(postData));
                callback();
        }).catch(error => console.error(error.message));
};

export const addPost = (post) => ({
    type: POST_ACTIONS.ADD,
    post
});

export const setActivePost = (postId) => ({
    type: POST_ACTIONS.SET_ACTIVE,
    postId
});

export const updatePost = (postData, callback) => {
    const request = { ...prepareRequest(postData), method: 'PUT'};
    const POST_URL = `${POSTS_URL_PREFIX}/${postData.id}`;

    fetchJson(POST_URL, request, callback);

    return {
        type: POST_ACTIONS.UPDATE,
        post: postData
    }
};

export const deletePost = (postId) => {
    fetchJson(`${POSTS_URL_PREFIX}/${postId}`, {
        method: 'DELETE'
    });

    return {
        type: POST_ACTIONS.DELETE,
        postId
    }
};

export const changeFilterText = (filterText) => ({
    type: POST_ACTIONS.CHANGE_FILTERING_TEXT,
    filterText
});

export const fetchPosts = (page, limit) => {
    return dispatch =>
        fetchJson(`${POSTS_URL_PREFIX}?_page=${page}&_limit=${limit}`, {}, data =>
            dispatch(receivePosts(data))
        );
};

export const receivePosts = posts => ({
    type: POST_ACTIONS.RECEIVE,
    posts
});
