export const prepareRequest = (form) => (
    {
        method: 'GET',
        mode: 'cors',
        body: JSON.stringify(form),
        headers: {
            'Content-Type': 'application/json; charset=utf-8',
        }
    }
);