export const fetchJson = (url, params, callback) =>
    fetch(url, params)
        .then(response => {
            return response.ok ? response.json() : response.text();
        })
        .then(callback);
