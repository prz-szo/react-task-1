import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Header from '../../components/Header/Header';
import AuthenticatedRoute from '../../components/AuthenticatedRoute/AuthenticatedRoute';
import LoginPage from '../../pages/LoginPage/LoginPage';
import MainPage from '../../pages/MainPage/MainPage';
import PostDetailPage from '../../pages/PostDetailPage/PostDetailPage';
import Footer from '../../components/Footer/Footer';
import { isAuthenticated } from '../../utils/isAuthenticated';

import './App.css';


const FOOTER_COPYRIGHT = 'Copyright 2017 ⓒ';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isAuthenticated: isAuthenticated()
        };
    }

    __onLoginSuccess = () => {
        this.setState({ isAuthenticated: isAuthenticated() })
    };

    render() {
        return (
            <Router>
                <div className="container">
                    <Header title="Logo" altText="Header logo"/>

                    <Route path="/login" render={(props) =>
                        <LoginPage {...props} onLoginSuccess={this.__onLoginSuccess} />
                    }/>
                    <AuthenticatedRoute isAuthenticated={this.state.isAuthenticated} exact path="/" component={MainPage} />
                    <AuthenticatedRoute isAuthenticated={this.state.isAuthenticated} path="/add" component={PostDetailPage} />
                    <AuthenticatedRoute isAuthenticated={this.state.isAuthenticated} path="/posts/:postId" component={PostDetailPage} />

                    <Footer text={FOOTER_COPYRIGHT}/>
                </div>
            </Router>
        );
    }
}

export default App;
