import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from "react-redux";

import Post from '../../components/Post/Post.js';
import Modal from '../../components/Modal/Modal';
import { deletePost, setActivePost } from '../../redux/modules/post/actionCreators';

import './FilterablePostList.css';


class FilterablePostList extends Component {
    static propTypes = {
        filterText: PropTypes.string,
        posts: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number.isRequired,
            userId: PropTypes.number.isRequired,
            title: PropTypes.string.isRequired,
            body: PropTypes.string
        })),
        activeUserId: PropTypes.number
    };

    constructor(props) {
        super(props);
        this.state = {
            promptVisible: false,
            postToDelete: null
        }
    }

    __createPostsComponents = phraseToSearch =>
        this.props.posts
            .filter(post =>
                post.title.toLowerCase().includes(phraseToSearch) ||
                post.body.toLowerCase().includes(phraseToSearch)
            )
            .map(post =>
                <Post key={post.id}
                      id={post.id}
                      title={post.title}
                      body={post.body}
                      onOpen={this.__onOpenPost}
                      className={ (post.userId === this.props.activeUserId) ? 'post-created-by-author' : null}
                      onDelete={this.__togglePrompt}
                />
            );

    __onOpenPost = (postId) => {
        this.props.onOpen(postId);
        this.props.history.push(`/posts/${postId}`);
    };

    __onDeletePost = () => {
        const postToDelete = this.state.postToDelete;
        this.props.onDelete(postToDelete);
        this.__togglePrompt();
    };

    __togglePrompt = (postId = null) =>
        this.setState({
            postToDelete: postId,
            promptVisible: !this.state.promptVisible
        });

    render() {
        const phraseToSearch = this.props.filterText.toLowerCase();
        const postsComponents = this.__createPostsComponents(phraseToSearch);

        return (
            <React.Fragment>
                <div className="filterable-post-list">
                    {postsComponents}
                </div>
                <Modal
                    when={this.state.promptVisible}
                    message="Are you sure this post should be deleted?"
                    submitButtonLabel="Delete"
                    onSubmit={this.__onDeletePost}
                    onCancel={this.__togglePrompt}
                />
            </React.Fragment>
        );
    }
}


const mapStateToProps = state => {
    const { filterText, posts } = state.post;

    return {
        filterText,
        posts,
        activeUserId: state.authors.activeUser.id
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onOpen: id => {
            dispatch(setActivePost(id))
        },
        onDelete: id => {
            dispatch(deletePost(id))
        }
    }
};

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(FilterablePostList));
