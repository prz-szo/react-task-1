import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Comment from '../../components/Comment/Comment.js';
import { fetchJson } from '../../utils/fetchJson.js';

import './CommentsList.css';


class CommentsList extends Component {
    static propTypes = {
        commentsUrl: PropTypes.string.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            comments: []
        };
    }

    componentDidMount() {
        fetchJson(this.props.commentsUrl, {}, (data) => {
            const comments = data.map((comment) =>
                     <Comment key={comment.id}
                              title={comment.name}
                              body={comment.body}
                     />
                );

            this.setState({ comments });
        });
    }

    render() {
        return (
            <div className="comments-list">
                <h3 className="comments-list-title">Comments</h3>
                {this.state.comments}
            </div>
        );
    }
}


export default CommentsList;
