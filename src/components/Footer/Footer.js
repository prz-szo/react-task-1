import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './Footer.css';


class Footer extends Component {
    static propTypes = {
        text: PropTypes.string
    };

    render() {
        return (
            <footer className="footer">{this.props.text}</footer>
        );
    }
}


export default Footer;
