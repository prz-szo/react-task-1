import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Button from '../Button/Button.js';

import './Post.css';


class Post extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired,
        body: PropTypes.string,
        id: PropTypes.number.isRequired,
        onDelete: PropTypes.func.isRequired,
        className: PropTypes.string
    };

    render() {
        return (
            <div className={classNames('post', this.props.className)}>
                <h1 className="post-title">{this.props.title}</h1>
                <Button label="Open" onClick={() => this.props.onOpen(this.props.id)}/>
                <div className="post-content">{this.props.body}</div>
                <Button label="Delete" onClick={() => this.props.onDelete(this.props.id)}/>
            </div>

        );
    }
}


export default Post;
