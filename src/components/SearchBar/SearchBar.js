import React, { Component } from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import debounce from 'lodash.debounce';

import TextInput from '../TextInput/TextInput.js';
import { changeFilterText } from "../../redux/modules/post/actionCreators";

import './SearchBar.css';


const DEBOUNCING_DELAY = 250;

class SearchBar extends Component {
    static propTypes = {
        changeFilterText: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            value: ''
        }
    }

    __onEnterTextDebounced = debounce((enteredText) => {
        this.props.changeFilterText(enteredText);
    }, DEBOUNCING_DELAY);

    __onChange = (e) => {
        const filterText = e.target.value;
        this.setState({ value: filterText });
        this.__onEnterTextDebounced(filterText);
    };

    render() {
        return (
            <div className="search-bar">
                <TextInput value={this.state.value}
                           onChange={this.__onChange}
                           placeholder="Search post"
                />
            </div>
        );
    }
}


const mapDispatchToProps = {
    changeFilterText
};


export default connect(
    null,
    mapDispatchToProps
)(SearchBar);
