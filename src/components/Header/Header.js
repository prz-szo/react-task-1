import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import Logo from '../Logo/Logo.js';
import Breadcrumbs from '../Breadcrumbs/Breadcrumbs';
import Button from '../Button/Button';
import { fetchJson } from '../../utils/fetchJson';
import { logOut } from '../../redux/modules/login/actionCreators';
import { fetchAuthorData, receiveAuthorData } from '../../redux/modules/authors/actionCreators';
import { isAuthenticated } from "../../utils/isAuthenticated";

import { LOGO_URL, POSTS_URL_PREFIX } from '../../config/consts';
import './Header.css';


class Header extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired,
        altText: PropTypes.string.isRequired,
        location: PropTypes.object.isRequired,
        activePost: PropTypes.number,
        activeUser: PropTypes.shape({
            id: PropTypes.number,
            firstName: PropTypes.string,
            lastName: PropTypes.string
        }),
        logOut: PropTypes.func.isRequired,
        fetchAuthorData: PropTypes.func.isRequired,
        clearAuthorData: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            headerImageSrc: '',
            routes: []
        };
    }

    componentDidMount() {
        this.__setImageSrc(LOGO_URL);
        this.__createRoutes();

        if(isAuthenticated()) {
            this.props.fetchAuthorData();
        }
    }

    componentDidUpdate(prevProps) {
        if(this.props.location.key !== prevProps.location.key) {
            this.__createRoutes();

            if(isAuthenticated()) {
                this.props.fetchAuthorData();
            }
        }
    }

    __setImageSrc = (url) =>
        fetchJson(url, {}, (data) => {
            const headerImageSrc = data.thumbnailUrl;
            this.setState({ headerImageSrc });
            });

    __createRoutes = () => {
        const pathname = this.props.location.pathname;

        switch(pathname) {
            case '/add':
                this.setState({ routes: [
                        { name: 'Add new', route: pathname }
                    ]});
                break;
            case '/':
            case '/login':
                this.setState({ routes: [
                        { name: 'Posts', route: '/' },
                    ]});
                break;
            default:
                const postId = this.props.activePost;
                fetchJson(`${POSTS_URL_PREFIX}/${postId}`, {}, (post) =>
                    this.setState({ routes: [
                            { name: 'Posts', route: '/' },
                            { name: `${post.title} (${postId})`, route: `${pathname}` }
                        ]})
                );
        }
    };

    __onLogOut = () => {
        this.props.logOut();
        this.props.clearAuthorData();
        this.props.history.push('/login');
    };

    render() {
        return (
            <header className="header">
                <Logo src={this.state.headerImageSrc} altText={this.props.altText}/>
                <h1 className="header-title">{this.props.title}</h1>
                {(this.props.activeUser.id) ?
                    (
                        <div className="header-info-group">
                            <span>Hi, {this.props.activeUser.firstName} {this.props.activeUser.lastName}</span>
                            <Button label="Logout" onClick={this.__onLogOut}/>
                            <Breadcrumbs separator=">" routes={this.state.routes}/>
                        </div>
                    ) : null
                }
            </header>
        );
    }
}


const mapStateToProps = state => (
  {
      activePost: state.post.activePost,
      activeUser: state.authors.activeUser
  }
);

const mapDispatchToProps = dispatch => {
  return {
      logOut: () => {
          dispatch(logOut());
      },
      fetchAuthorData: () => {
          dispatch(fetchAuthorData());
      },
      clearAuthorData: () => {
          dispatch(receiveAuthorData({}))
      }
  }
};


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));
