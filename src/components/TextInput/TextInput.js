import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './TextInput.css';


class TextInput extends Component {
    static propTypes = {
        type: PropTypes.string,
        value: PropTypes.string,
        onChange: PropTypes.func.isRequired,
        placeholder: PropTypes.string.isRequired,
        errors: PropTypes.arrayOf(PropTypes.string)
    };

    static defaultProps = {
        type: 'text',
        value: '',
        errors: []
    };

    render() {
        const inputClassNames = classNames("text-input", {"invalid": this.props.errors.length});
        return (
            <React.Fragment>
                <span className="error">{this.props.errors}</span>
                <input type={this.props.type}
                       className={inputClassNames}
                       value={this.props.value}
                       placeholder={this.props.placeholder}
                       onChange={this.props.onChange}
                />
            </React.Fragment>
        );
    }
}


export default TextInput;
