import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import Button from '../Button/Button';
import TextInput from '../TextInput/TextInput';
import TextArea from '../TextArea/TextArea.js';
import RadioBoxGroup from '../RadioBoxGroup/RadioBoxGroup.js';
import { clearErrors, validate } from '../../redux/modules/form/actionCreators';
import { createPost, updatePost, setActivePost } from '../../redux/modules/post/actionCreators';

import { VALIDATION_TYPES } from '../../redux/actions';
import './EditPostForm.css';


class EditPostForm extends Component {
    static propTypes = {
        id: PropTypes.number,
        userId: PropTypes.number,
        title: PropTypes.string,
        body: PropTypes.string,
        users: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number.isRequired,
            name: PropTypes.string.isRequired
        })).isRequired,
        errors: PropTypes.shape({
            title: PropTypes.arrayOf(PropTypes.string),
            body: PropTypes.arrayOf(PropTypes.string),
            userId: PropTypes.arrayOf(PropTypes.string)
        })
    };

    constructor(props) {
        super(props);

        this.state = {
            title: this.props.title,
            body: this.props.body,
            userId: this.props.userId
        };
    }

    __onSaveChanges = () => {
        const fieldsToValidate = [
            {fieldId: 'userId', type: VALIDATION_TYPES.RADIOGROUP, value: this.state.userId},
            {fieldId: 'title', type: VALIDATION_TYPES.INPUT, value: this.state.title},
            {fieldId: 'body', type: VALIDATION_TYPES.INPUT, value: this.state.body}
        ];

        if(!this.props.validate(fieldsToValidate)) {
            return null;
        }

        const postData = {
            id: this.props.id,
            userId: this.state.userId,
            title: this.state.title,
            body: this.state.body
        };

        if (this.props.id) {
            this.props.updatePost(postData, this.__backToMainPage);
        } else {
            this.props.createPost(postData, this.__backToMainPage);
        }
    };

    __backToMainPage = () => {
        this.props.history.push('/');
    };

    __onCancel = () => {
        this.props.clearActivePostId();
        this.props.clearErrors();
        this.__backToMainPage();
    };

    render() {
        return (
            <div className="edit-post-form">
                <h3 className="edit-post-form-title">Edit / Insert Post</h3>
                <TextInput placeholder="Title"
                           value={this.state.title}
                           onChange={e => this.setState({ title: e.target.value })}
                           errors={this.props.errors.title}
                />
                <TextArea placeholder="Content"
                          value={this.state.body}
                          onChange={body => this.setState({ body })}
                          errors={this.props.errors.body}
                />
                <RadioBoxGroup title="Users"
                               checked={this.state.userId}
                               options={this.props.users}
                               onChange={userId => this.setState({ userId })}
                               errors={this.props.errors.userId}
                />

                <Button label="Cancel" onClick={this.__onCancel}/>
                <Button label="Save changes" onClick={this.__onSaveChanges}/>
            </div>
        );
    }
}


const mapStateToProps = state => {
    const users = state.authors.users.map(user =>
        ({
            id: user.id,
            name: `${user.firstName} ${user.lastName}`
        })
    );

    const activePost = state.post.posts.find((post) => post.id === state.post.activePost);
    const errors = state.form.errors;

    return activePost ?
        { ...activePost, users, errors } : { users, errors };
};

const mapDispatchToProps = dispatch => {
    return {
        clearActivePostId: () => {
            dispatch(setActivePost(null))
        },
        clearErrors: () => {
            dispatch(clearErrors());
        },
        updatePost: (postData, callback) => {
            dispatch(updatePost(postData, callback))
        },
        createPost: (postData, callback) => {
            dispatch(createPost(postData, callback))
        },
        validate: fieldsToValidate =>
            dispatch(validate(fieldsToValidate))
    }
};

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(EditPostForm));
