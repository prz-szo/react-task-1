import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Button from '../Button/Button';

import './Modal.css';


class Modal extends Component{
    static propTypes = {
        when: PropTypes.bool.isRequired,
        message: PropTypes.string.isRequired,
        submitButtonLabel: PropTypes.string.isRequired,
        onSubmit: PropTypes.func.isRequired,
        onCancel: PropTypes.func.isRequired
    };

    render() {
        if(!this.props.when) {
            return null;
        }

        return (
            <div className="modal-overlay">
                <div className="modal">
                    {this.props.message}
                    <div className="modal-actions">
                        <Button label="Cancel" onClick={() => this.props.onCancel()}/>
                        <Button label={this.props.submitButtonLabel} onClick={() => this.props.onSubmit()}  />
                    </div>
                </div>
            </div>
        );
    }
}

export default Modal;
