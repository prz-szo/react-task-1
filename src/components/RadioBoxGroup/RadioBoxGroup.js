import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './RadioBoxGroup.css';
import classNames from 'classnames';


class RadioBoxGroup extends Component {
    static propTypes = {
        onChange: PropTypes.func.isRequired,
        options: PropTypes.arrayOf(PropTypes.shape({
            name: PropTypes.string.isRequired,
            id: PropTypes.number.isRequired
        })).isRequired,
        checked: PropTypes.number,
        errors: PropTypes.arrayOf(PropTypes.string)
    };

    __createOptions = () =>
        this.props.options.map((option) =>
            <React.Fragment key={option.id}>
                <input type="radio"
                       value={option.id}
                       name="options"
                       checked={option.id === this.props.checked}
                       onChange={this.__chooseOption}
                />
                {option.name}<br/>
            </React.Fragment>
        );

    __chooseOption = (e) =>
        this.props.onChange(parseInt(e.target.value, 10));

    render() {
        const options = this.__createOptions();
        const radioBoxClassNames = classNames("radio-box", {"invalid": this.props.errors && this.props.errors.length});

        return (
            <React.Fragment>
                <span className="error">{this.props.errors}</span>
                <div className="radio-box-group">
                    <span>{this.props.title}</span>
                    <div className={radioBoxClassNames}>
                        {options}
                    </div>
                </div>
            </React.Fragment>
        );
    }
}


export default RadioBoxGroup;
