import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './Comment.css';

class Comment extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired,
        body: PropTypes.string.isRequired
    };

    render() {
        return (
            <div className="comment">
                <h3 className="comment-title">{this.props.title}</h3>
                <div className="comment-body">{this.props.body}</div>
            </div>
        );
    }
}


export default Comment;
