import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './Button.css';


class Button extends Component {
    static propTypes = {
        label: PropTypes.string.isRequired,
        onClick: PropTypes.func
    };

    render() {
        return (
            <button className="button" onClick={this.props.onClick}>{this.props.label}</button>
        );
    }
}


export default Button;
