import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './TextArea.css';


class TextArea extends Component {
    static propTypes = {
        value: PropTypes.string,
        onChange: PropTypes.func.isRequired,
        placeholder: PropTypes.string.isRequired,
        errors: PropTypes.arrayOf(PropTypes.string)
    };

    render() {
        const textAreaClassNames = classNames("text-area", {"invalid": this.props.errors && this.props.errors.length});
        return (
            <React.Fragment>
                <span className="error">{this.props.errors}</span>
                <textarea className={textAreaClassNames}
                          value={this.props.value}
                          placeholder={this.props.placeholder}
                          onChange={(e) => this.props.onChange(e.target.value)}
                />
            </React.Fragment>
        );
    }
}


export default TextArea;
