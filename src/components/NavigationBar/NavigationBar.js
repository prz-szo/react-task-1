import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import SearchBar from '../../components/SearchBar/SearchBar.js'
import Button from '../../components/Button/Button.js';

import './NavigationBar.css';


class NavigationBar extends Component {
    static propTypes = {
        link: PropTypes.string.isRequired
    };

    render() {
        return (
            <div className="nav-bar">
                <SearchBar />
                <Link to={this.props.link} className="link">
                    <Button label="Add post" />
                </Link>
            </div>
        );
    }
}


export default NavigationBar;
