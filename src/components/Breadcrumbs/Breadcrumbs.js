import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import './Breadcrumbs.css';


class Breadcrumbs extends Component {
    static propTypes = {
        routes: PropTypes.arrayOf(PropTypes.shape({
            name: PropTypes.string.isRequired,
            route: PropTypes.string.isRequired
        })).isRequired,
        separator: PropTypes.string.isRequired
    };

    __makeCrumbs = () => (
        this.props.routes
            .map((link) =>
                <React.Fragment key={link.route}>
                    {` ${this.props.separator} `}
                    <Link to={link.route} >
                        {link.name}
                    </Link>
                </React.Fragment>
            )
    );

    render() {
        const crumbs = this.__makeCrumbs();

        return (
            <div className="breadcrumbs-container">
                <div className="breadcrumbs">
                    {crumbs}
                </div>
            </div>
        );
    }
}


export default Breadcrumbs;
