import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './Logo.css';


class Logo extends Component {
    static propTypes = {
        src: PropTypes.string.isRequired,
        altText: PropTypes.string.isRequired
    };

    render() {
        return (
            <img src={this.props.src} alt={this.props.altText} className="logo"/>
        );
    }
}


export default Logo;
