import React, { Component } from 'react';
import PropTypes from 'prop-types';

import CommentsList from '../../containers/CommentsList/CommentsList';
import EditPostForm from '../../components/EditPostForm/EditPostForm.js';

import { POSTS_URL_PREFIX } from '../../config/consts';
import './PostDetailPage.css';


class PostDetailPage extends Component {
    static propTypes = {
        match: PropTypes.object.isRequired
    };

    render() {
        const NOT_EXSISTING_POST_ID = 0;

        const postId = parseInt(this.props.match.params.postId, 10) || NOT_EXSISTING_POST_ID;
        const COMMENTS_URL = `${POSTS_URL_PREFIX}/${postId}/comments`;

        return (
            <React.Fragment>
                <EditPostForm />
                <CommentsList commentsUrl={COMMENTS_URL}/>
            </React.Fragment>
        );
    }
}

export default PostDetailPage;
