import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import TextInput from '../../components/TextInput/TextInput';
import Button from '../../components/Button/Button';
import { checkCredentials } from '../../redux/modules/login/actionCreators';

import './LoginPage.css';


class LoginPage extends Component {
    static propTypes = {
        errors: PropTypes.arrayOf(PropTypes.string),
        onLoginSuccess: PropTypes.func.isRequired,
        checkCredentials: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            login: '',
            password: ''
        }
    }

    __onSignIn = () => {
        this.props.checkCredentials(this.state, () => {
            this.props.onLoginSuccess();
            this.props.history.push('/')
        });
    };

    render() {
        return (
            <div className="login-page">
                <span className="login-page-header">Hello, let me know you Stranger</span>
                <hr className="login-page-line"/>
                <TextInput value={this.state.login}
                           onChange={e => this.setState({ login: e.target.value })}
                           placeholder="E-mail"
                           errors={this.props.errors}
                />
                <TextInput value={this.state.password}
                           onChange={e => this.setState({ password: e.target.value })}
                           placeholder="Password"
                           type="password"
                           errors={this.props.errors}
                />
                <hr className="login-page-line" />
                <Button onClick={this.__onSignIn} label="Sign in" />
            </div>
        );
    }
}


const mapStateToProps = state => {
    return {
        errors: state.login.errors
    }
};

const mapDispatchToProps = {
    checkCredentials
};


export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
