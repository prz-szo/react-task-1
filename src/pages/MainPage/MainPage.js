import React, { Component } from 'react';
import { connect } from 'react-redux';

import NavigationBar from '../../components/NavigationBar/NavigationBar';
import FilterablePostList from '../../containers/FilterablePostList/FilterablePostList';
import { fetchAuthors } from '../../redux/modules/authors/actionCreators';
import { fetchPosts } from '../../redux/modules/post/actionCreators';

import './MainPage.css';



class MainPage extends Component {
    componentDidMount() {
        const INITIAL_POSTS_PAGE = 1;
        const POSTS_LIMIT_ON_PAGE = 30;

        this.props.fetchPosts(INITIAL_POSTS_PAGE, POSTS_LIMIT_ON_PAGE);
        this.props.fetchAuthors();
    }

    render() {
        return (
            <React.Fragment>
                <NavigationBar link="/add" />
                <FilterablePostList />
            </React.Fragment>
        );
    }
}

const mapDispatchToProps = {
    fetchPosts,
    fetchAuthors
};

export default connect(null, mapDispatchToProps)(MainPage);
